package com.ahmet.testinstagram.CustomViews;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ahmet.testinstagram.Constants;
import com.ahmet.testinstagram.Interfaces.AuthenticationListener;
import com.ahmet.testinstagram.R;

/**
 * Created by elifyesiltas on 8.12.2018.
 */

public class AuthenticationDialog extends Dialog {
    AuthenticationListener listener;

    WebView webView;

    String url = Constants.BASE_URL
            +"oauth/authorize/?client_id="
            +Constants.CLIENT_ID
            +"&redirect_uri="
            +Constants.REDIRECT_URI
            +"&response_type=token"
            +"&display=touch&scope=public_content";
    public AuthenticationDialog(@NonNull Context context, AuthenticationListener listener) {
        super(context);
        this.listener=listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.auth_dialog);
        initializeWebview();
    }

    private void initializeWebview() {
        webView=findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setAppCacheEnabled(true);
        webSettings.setAppCacheMaxSize(100 * 1000 * 1000);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient(){
            String access_token;
            boolean authComplete;
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (url.contains("#access_token=") && !authComplete){
                    Uri uri=Uri.parse(url);
                    access_token=uri.getEncodedFragment();
                    access_token=access_token.substring(access_token.lastIndexOf("=")+1);
                    Log.e("dialog", "token: "+access_token );
                    authComplete=true;
                    listener.onCodeReceived(access_token);
                    dismiss();
                }else if (url.contains("?error")){
                    Log.e("dialog", "error: access token error fetching");
                    dismiss();
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        });
    }
}
