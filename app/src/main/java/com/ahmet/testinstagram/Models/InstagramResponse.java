package com.ahmet.testinstagram.Models;

/**
 * Created by elifyesiltas on 8.12.2018.
 */

public class InstagramResponse {

    Data[] data;

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }
}
