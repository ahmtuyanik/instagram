package com.ahmet.testinstagram;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ahmet.testinstagram.CustomViews.AuthenticationDialog;
import com.ahmet.testinstagram.Interfaces.AuthenticationListener;

public class MainActivity extends AppCompatActivity implements AuthenticationListener {

    AuthenticationDialog auth_dailog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onCodeReceived(String auth_token) {
        if (auth_token==null)
            return;

        //Toast.makeText(this, "Yeeey "+auth_token, Toast.LENGTH_SHORT).show();
    }

    public void after_click_login(View v){
        auth_dailog=new AuthenticationDialog(this,this);
        auth_dailog.setCancelable(true);
        auth_dailog.show();
    }
}
