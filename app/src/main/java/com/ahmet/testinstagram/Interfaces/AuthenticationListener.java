package com.ahmet.testinstagram.Interfaces;

/**
 * Created by elifyesiltas on 8.12.2018.
 */

public interface AuthenticationListener {
    void onCodeReceived(String auth_token);
}
