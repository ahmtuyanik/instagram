package com.ahmet.testinstagram.Models;

/**
 * Created by elifyesiltas on 8.12.2018.
 */

public class User {
    String profile_picture;
    String full_name;

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
