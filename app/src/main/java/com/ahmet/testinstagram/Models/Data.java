package com.ahmet.testinstagram.Models;

/**
 * Created by elifyesiltas on 8.12.2018.
 */

public class Data {

    Images images;
    User user;

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
